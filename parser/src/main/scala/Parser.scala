trait ParseTo[-A, +B] {
  def to(a: A): B
}
object ParseTo {
  def apply[A, B](implicit parseTo: ParseTo[A, B]): A ParseTo B = parseTo
}
sealed trait ParseToSyntax {
  implicit final def parseToOps[A](a: A):ParseToOps[A] = new ParseToOps(a)
}
final class ParseToOps[A](private val a: A) extends AnyVal {
  def to[B: ParseTo[A, *]]: B = ParseTo[A, B].to(a)
  def toV[B](implicit p: ParseTo[A, B]): B = ParseTo[A, B].to(a)
}
object parser extends ParseToSyntax