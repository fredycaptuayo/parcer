object Main extends App {
  implicit def parse: ParseTo[MyInt, MyString] = ???
  val myInt: MyInt = MyInt(10) // f: MyInt => MyString == ParseTo[MyInt, MyString]
  // ParseTo[MyString, MyInt]
  // ParseTo[List[MyInt], List[MyString]]
  val r: MyString = myInt.to[MyString]
  println(r)
}

final case class MyInt(value: Int)
final case class MyString(value: String)